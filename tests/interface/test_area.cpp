/**
 *
 * @file tests/interface/test_area.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 * \brief Test of the container drawing
 */

#include "test_area.hpp"

using namespace std;

class Interface_ : public Interface
{
public:
    void error(const string &s) const {
        cerr << s << endl;
    }

    void warning(const string &s) const {
        cerr << s << endl;
    }

    void information(const string &s) const {
        cerr << s << endl;
    }

    const std::string get_filename() const {
        return string("Filename");
    }
};

int main(int argc, char **argv) {
    glutInit(&argc, argv);

    QApplication app(argc, argv);

    Interface_ i;
    Message::set_interface(&i);

    //  Interface_graphic* g = new Interface_graphic();
    QWidget t;
    t.setGeometry(0, 0, 800, 600);

    // Render_area r(&t);
    // r.setGeometry(0, 0, 800, 600);
    //     r.show();
    t.show();

    // r.start_draw();
    {

        // r.start_draw_containers();
        {
            /*  r.draw_container(0, 0, 30, 20);
                r.draw_container(0, 22, 10, 20);
                r.draw_container(0, 43, 15, 20);*/
            //  r.draw_container(1, 60, 60, 20);
            // r.draw_container_text(0, 10,"Container 2");
            // r.draw_container_text(0, 32,"Container 1");
        }
        // r.end_draw_containers();

        // r.start_draw_states();
        {

            /*            r.draw_state(0, 4, 0, 20, 0.6, 0.7, 0.8);
                          r.draw_state(4.5, 7.98, 0, 20, 0.6, 0.7, 0.8);
                          r.draw_arrow(10, 10, 0, 3);
                          r.draw_arrow(10, -10, -15, -3);
                          r.draw_arrow(-150, 150, -10, 30);
                          r.draw_event(14, 4, 2);
                          r.draw_event(10, 4, 1);
                          r.draw_event(18, 4, 3);
                          r.draw_event(10, 4, 10);*/
        }
        // r.end_draw_states();

        // r.start_draw_counter();
        {
            // r.draw_counter(0.0f, 1);
            // r.draw_counter(5, 1.2);
            // r.draw_counter(7, 2);
            // r.draw_counter(19.345, 3);
            // r.draw_counter(24.45, 2);
            // r.draw_counter(32, 6);
        }
        // r.end_draw_counter();
    }
    // r.end_draw();

    // r. build();

    return app.exec();
}
