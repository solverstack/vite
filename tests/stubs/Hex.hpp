/**
 *
 * @file tests/stubs/Hex.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef HEX_HPP
#define HEX_HPP

#include <iostream>
#include "Value.hpp"

/*!
 *
 * \file hex.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */

class Hex : public Value{
private:
    std::string me;

public:
    Hex();
    std::string to_string() const;
    /*!
     *
     * \fn instantiate(const std::string &in,  &out)
     * \brief Convert a string to a Hex
     * \param in String to convert
     * \param out Hex to be initialized
     * \return true, if the conversion succeeded
     *
     */
    static bool instantiate(const std::string &in, Hex &out);
};


#endif // HEX_HPP
