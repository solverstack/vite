/**
 *
 * @file tests/stubs/Name.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef NAME_HPP
#define NAME_HPP

#include <iostream>
#include "Value.hpp"
#include "String.hpp"
/*!
 *
 * \file name.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */
class Name : public Value
{
private:
    std::string me;

public:
    Name();
    std::string to_string() const;

    void set_alias(const std::string &alias);
    void set_name(const std::string &name);

    String to_String();
};

#endif // NAME_HPP
