/**
 *
 * @file tests/plugin/Plugin.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef PLUGIN_HPP
#define PLUGIN_HPP

#include <string>
#include <map>
#include <QWidget>
#include <QVariant>

class Plugin : public QWidget {
private:
    std::string _name;
public:
    virtual void init() = 0;
    virtual void execute() = 0;
    virtual std::string get_name() = 0;
    virtual void set_arguments(std::map<std::string /*argname*/, QVariant */*argValue*/>) = 0; /* Voir comment ca passe :p */
    
    static Plugin *new_instance(const std::string &name);
};

#endif // PLUGIN_HPP
