/**
 *
 * @file tests/parser/test_parser_paje.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Pascal Noisette
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "../../src/parser/ParserPaje.hpp"
#include "../../src/parser/Parser.hpp"
#include "../stubs/Trace.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main(){

   
    Trace trace;
    Parser *p = new ParserPaje();
    p->parse("trace_to_parse.trace",trace);
  
    delete p;
    return 0;
}
