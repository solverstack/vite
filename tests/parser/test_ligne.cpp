/**
 *
 * @file tests/parser/test_ligne.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Pascal Noisette
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "../../src/parser/Line.hpp"
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char **argv){

    Line line;
    if (argc<2)
      line.open("trace_to_parse.trace");
    else
      line.open(argv[1]);
    int linecount = 0;
   

    while(!line.is_eof()){

	line.newline();

	linecount++;

	line.print();
    }

    cout << "lu :" << linecount << endl;

    return 0;
}
