/**
 *
 * @file tests/generator/main.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include <QtGui/QApplication>
#include "GeneratorWindow.hpp"


int main (int argc, char** argv){
    QApplication* app = new QApplication(argc,argv);
    GeneratorWindow* g = new GeneratorWindow (NULL, app);
    g->run ();

    return EXIT_SUCCESS;
}
