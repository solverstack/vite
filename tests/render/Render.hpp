/**
 *
 * @file tests/render/Render.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
#ifndef RENDER_HPP
#define RENDER_HPP

#include<iostream>


template<class T>
class Render{

    T* render_instance;
public:
    Render(T* instance){
        std::cout << "Render constructor" << std::endl;
        render_instance = instance;
    }

    void draw(){
        std::cout << "Render draw" << std::endl;
        render_instance->drawRect();
    }

};

#endif 
