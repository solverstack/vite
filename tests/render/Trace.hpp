/**
 *
 * @file tests/render/Trace.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
#ifndef TRACE_HPP
#define TRACE_HPP

#include "Render.hpp"
#include "Render_opengl.hpp"

class Trace{
public:
    Trace(){
        std::cout << "Trace constructor" << std::endl;
    }

    template <class B> 
    void build(B r){
        std::cout << "Trace build" << std::endl;
        r.draw();
    }

};

#endif 
