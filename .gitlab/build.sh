#!/usr/bin/env bash
###
#
#  @file build.sh
#  @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.4.0
#  @author Mathieu Faverge
#  @author Florent Pruvost
#  @date 2023-12-07
#
###
set -ex

cmake -B build -S . -DVITE_CI_VERSION=${VERSION} -DVITE_CI_BRANCH=${BRANCH} -C .gitlab/ci-test-initial-cache.cmake
cp build/compile_commands.json build/compile_commands-${VERSION}.json
cmake --build build -j 4  > /dev/null
cmake --install build
