/**
 *
 * @file src/main.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 * \file main.cpp
 * \brief The main launcher.
 */

#include <QApplication>
#include <QObject>
#include <QCoreApplication>
#include <QMessageBox>
/* -- */
#include <cstdio>
#include <cstdlib>
#include <string>
#include <stack>
/* -- */
/* Global information */
#include "common/common.hpp"
#ifdef MEMORY_USAGE
#include "common/Memory.hpp"
#endif
/* -- */
#include "common/Info.hpp"
#include "core/Core.hpp"

/*!
 *\brief The main function of ViTE.
 */
int main(int argc, char **argv) {

#ifdef VITE_DBG_MEMORY_TRACE
    FILE *file;
    double timestamp;
#endif

    Q_INIT_RESOURCE(vite);

    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    // Display an error if the user uses Wayland
    if (getenv("WAYLAND_DISPLAY")) {
        QApplication app(argc, argv);
        QMessageBox::critical(nullptr, QString::fromStdString("Wayland ERROR"), QString::fromStdString("ViTE does not work (for now) on a Wayland environment.\nSwitch to an X environment to use ViTE (you can usually do it when selecting your desktop environment when you login on your machine)."));
        return EXIT_SUCCESS;
    }

#ifdef VITE_DBG_MEMORY_TRACE
    timestamp = clockGet();
    file = fopen("toto.trace", "w");
    memAllocTrace(file, timestamp, 0);
    trace_start(file, timestamp, 0, -1);
#endif /* VITE_DBG_MEMORY_TRACE */

    Core console(argc, argv);
    console.run();

#ifdef MEMORY_USAGE
#ifdef VITE_DBG_MEMORY_TRACE
    trace_finish(file, (clockGet() - timestamp), 0, -1);
    memAllocUntrace();
#endif
    fprintf(stdout, "Max Memory allocated   : %lu\n", memAllocGetMax());
    fprintf(stdout, "Memory still allocated : %lu\n", memAllocGetCurrent());
#endif

    return EXIT_SUCCESS;
}
