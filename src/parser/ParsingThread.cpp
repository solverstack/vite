/**
 *
 * @file src/parser/ParsingThread.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */

#include "trace/Trace.hpp"
/* -- */
#include "parser/Parser.hpp"
/* -- */
#include "parser/ParsingThread.hpp"

ParsingThread::ParsingThread(Parser *p,
                             Trace *t) :
    _parser(p),
    _trace(t) { }

void ParsingThread::run() {
    try {
        /* Do not finish the build inside the parser,
         * we will do this after, if we don't want to
         * just dump the trace
         */
        _parser->parse(*_trace, false);

        /* set the parser's finished flag to true */
        if (_parser->is_canceled()) {
            finish_build();
            return;
        }

        _parser->finish();

    } catch (const std::string &) {
        _parser->finish();
    }
}

void ParsingThread::finish_build() {
    // Finished properly the trace building
    // Called if the parsing is cancelled or finished when splitting is disabled
    _trace->finish();
}

void ParsingThread::dump(const std::string &path,
                         const std::string &filename) {

#if defined(USE_ITC) && defined(BOOST_SERIALIZE)
    _trace->dump(path, filename);
#else
    (void)path;
    (void)filename;
#endif
}

#include "moc_ParsingThread.cpp"
