/**
 *
 * @file src/parser/ParserFactory.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Francois Trahay
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */

#include <string>
#include <list>
#include <map>
#include <fstream>
#include <sstream>
/* -- */
#include "common/ViteConfig.hpp"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "common/Message.hpp"
/* -- */
#include "parser/Parser.hpp"

#ifdef MT_PARSING
#include "parser/PajeParser/mt_ParserPaje.hpp"
#else
#include "parser/PajeParser/ParserPaje.hpp"
#endif

#include "parser/PajeParser/ParserVite.hpp"

#ifdef WITH_OTF2
#include <otf2/otf2.h>
#include "parser/OTF2Parser/ParserOTF2.hpp"
#endif // WITH_OTF2

#ifdef VITE_ENABLE_TAU
#include <TAU_tf.h>
#include "parser/TauParser/ParserTau.hpp"
#endif // VITE_ENABLE_TAU

#ifdef BOOST_SERIALIZE
#include "ParserSplitted.hpp"
#endif

#include "ParserFactory.hpp"

using namespace std;

bool ParserFactory::create(Parser **parser,
                           const string &filename) {
    size_t pos = filename.find_last_of('.');
    string ext = "";

    if (pos != string::npos) {
        ext = filename.substr(pos);

        if (ext == ".trace") {
            /* WARNING: Why MT parsing has a different name ? it should just replace the other ones */
#ifdef MT_PARSING
            *parser = new mt_ParserPaje(filename);
#else
            *parser = new ParserPaje(filename);
#endif
        }
        else if (ext == ".ept") {
            *parser = new ParserVite(filename);
        }
        else if (ext == ".otf2") {
#ifdef WITH_OTF2
            *parser = new ParserOTF2(filename);
#else
            *Message::get_instance() << "OTF2 parser was not compiled." << Message::endw;
            return false;
#endif // WITH_OTF2
        }
        else if ((ext == ".trc") || (ext == ".edf")) {
#ifdef VITE_ENABLE_TAU
            *parser = new ParserTau(filename);
#else
            *Message::get_instance() << "Tau parser was not compiled." << Message::endw;
            return false;
#endif // VITE_ENABLE_TAU
        }
        else if (ext == ".vite") {
#ifdef BOOST_SERIALIZE
            *parser = new ParserSplitted(filename);
#else
            *Message::get_instance() << "Boost serialization was not compiled. We cannot parse .vite files" << Message::endw;
            return false;
#endif // BOOST_SERIALIZE
        }
    }

    if (*parser == nullptr) {
#ifdef MT_PARSING
        *parser = new mt_ParserPaje(filename);
#else
        *parser = new ParserPaje(filename);
#endif
    }

    return true;
}
