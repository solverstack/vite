/**
 *
 * @file src/parser/ParserSplitted.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 * \file ParserPaje.hpp
 * \brief The implementation of Parser for Paje traces.
 */

#ifndef PARSERSPLITTED_HPP
#define PARSERSPLITTED_HPP

#include <cstdio>
#include <QtCore>
#include <parser/Parser.hpp>
#include <parser/PajeParser/PajeFileManager.hpp>

class ParserDefinitionPaje;
class ParserEventPaje;
class PajeFileManager;

/*!
 *
 * \class ParserPaje
 * \brief parse the input data format of Paje.
 *
 */
class ParserSplitted : public Parser
{
    int _loaded_itc;
    int _nb_itc;

public:
    /*!
     *  \fn ParserPaje()
     */
    ParserSplitted();
    ParserSplitted(const std::string &filename);

    /*!
     *  \fn ~ParserPaje()
     */
    ~ParserSplitted();

    /*!
     *  \fn parse(Trace &trace)
     *  \param trace : the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(Trace &trace,
               bool finish_trace_after_parse = true);

    void releasefile();

    /*!
     *  \fn get_percent_loaded() const
     *  \brief return the size of the file already read.
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    float get_percent_loaded() const;

    /*!
     * Return the parser of definitions.
     *
     */
    ParserDefinitionPaje *get_parser_def() const;

Q_SIGNALS:
    void produced(int i, PajeLine *line);
    void build_finish();
};

#endif // ParserSplitted_HPP
