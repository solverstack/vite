/**
 *
 * @file src/parser/Parser.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file Parser.hpp
 *\brief This file contains the abstract layer for the parser.
 */

#ifndef PARSER_HPP
#define PARSER_HPP

#include <queue>
#include <string>

class QEventLoop;
class Trace;
#include <QObject>
/*!
 * \class Parser Parser.hpp "../parser/src/Parser.hpp"
 * \brief Contains the definition of the parser interface.
 */

class Parser : public QObject
{
    Q_OBJECT

protected:
    bool _is_finished;
    bool _is_canceled;

    QEventLoop *_parsing_event_loop;

    // list of the files to parse
    std::queue<std::string> _q;

    /*!
     *  \fn const std::string get_next_file_to_parse()
     *  \brief return the name of the next file to parse.
     */
    const std::string get_next_file_to_parse();

public:
    Parser();
    Parser(const std::string &filename);
    ~Parser() override = default;

    /*!
     *  \fn parse(const std::string &filename, Trace &trace, bool finish_trace_after_parse = true)
     *  \param filename the name of the file to parse
     *  \param trace the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(const std::string &filename, Trace &trace, bool finish_trace_after_parse = true);

    /*!
     *  \fn parse(Trace &trace, bool finish_trace_after_parse = true) = 0
     *  \param trace the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    virtual void parse(Trace &trace, bool finish_trace_after_parse = true) = 0;

    /*!
     *  \fn get_percent_loaded() const = 0;
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    virtual float get_percent_loaded() const = 0;

    /*!
     *  \fn is_end_of_parsing() const
     *  \brief true is the parsing is finished
     *  \return boolean
     */
    virtual bool is_end_of_parsing() const;

    /*!
     * \fn set_canceled()
     * \brief called when the loading of the trace is canceled by the user
     */
    virtual void set_canceled();

    /*!
     * \fn is_canceled()
     * \brief return true if the parser was cancelled during parsing, otherwise false
     */
    bool is_canceled() const;

    /*!
     *  \fn set_file_to_parse(const std::string &filename)
     *  \param filename : the name of the file to parse
     *  \brief set the name of the file to parse.
     */
    virtual void set_file_to_parse(const std::string &filename);

    /*!
     *  \fn finish()
     *  \brief set the end of the parsing
     */
    virtual void finish();

    /*!
     * \brief Setter for the parsing event loop
     */
    void set_event_loop(QEventLoop *event_loop);

Q_SIGNALS:

    /*!
     * \brief Stops the main thread event loop happening during parsing
     */
    void stop_parsing_event_loop();
};

#endif // PARSER_HPP
