/**
 *
 * @file src/parser/ParsingUpdateThread.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */
#ifndef PARSING_UPDATE_THREAD_HPP
#define PARSING_UPDATE_THREAD_HPP

#include <QElapsedTimer>
#include <QThread>

class Parser;
class QTimer;

/*!
 * \class ParsingUpdateThread
 * \brief Thread to update parsing progression bar without using main GUI thread for that
 */
class ParsingUpdateThread : public QThread
{
    Q_OBJECT

private:
    Parser *_parser;
    QTimer *_update_timer;
    QElapsedTimer _elapse_timer;

protected:
    /*!
     * \brief Override Qthread run function
     */
    void run() override;

public:
    /*!
     *  \fn ParsingUpdateThread(ParserThread *pt)
     *  \param pt the parser thread from which the updates should come.
     */
    ParsingUpdateThread(Parser *pt, QTimer *update_timer);

private Q_SLOTS:
    /*!
     * \brief Called every second to update the progress bar
     * This is needed to pass arguments to update_progress
     */
    void update_slot();

Q_SIGNALS:
    void update_progress(const int time_elapsed, const float percent_loaded);
};
#endif // PARSING_THREAD_HPP
