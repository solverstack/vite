/**
 *
 * @file src/parser/PajeParser/BuilderThread.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef BUILDING_THREAD_HPP
#define BUILDING_THREAD_HPP

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QSemaphore>
#include <queue>
#include <parser/PajeParser/mt_PajeFileManager.hpp>

class Trace;
class Parser;
class mt_ParserEventPaje;
struct Trace_builder_struct;
/*!
 * \class ParsingThread
 * \brief Thread to parse asynchronously a trace with any parser available
 */
class BuilderThread : public QObject
{
    Q_OBJECT

private:
    mt_ParserEventPaje *_event_parser;
    Parser *_parser;
    std::queue<PajeLine *> *_q;
    QWaitCondition *_cond;
    QWaitCondition *_trace_cond;
    QMutex *_mutex;
    QMutex *_mutex2;
    QSemaphore *_freeSlots;
    QSemaphore *_linesProduced;

    Trace *_trace;
    bool _is_finished;

public:
    /*!
     *  \fn ParsingThread(Parser *p, Trace *t)
     *  \param p the event parser used to parse the file.
     *  \param t the trace where we store data.
     *  \param parser the parser used to parse the file.
     */
    BuilderThread(mt_ParserEventPaje *p, Trace *trace, QWaitCondition *cond,
                  QWaitCondition *trace_cond, QSemaphore *freeBytes,
                  QSemaphore *linesProduced, QMutex *mutex, QMutex *mutex2,
                  Parser *parser);

    bool is_finished();

public Q_SLOTS:
    void finish_build();

    /*!
     *  \fn run()
     *  \brief run the thread.
     */
    void run(unsigned int n, PajeLine *line);

Q_SIGNALS:
    void build_finished();
    void build_trace(int, Trace_builder_struct *);
};
#endif // PARSING_THREAD_HPP
