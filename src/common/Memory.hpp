/**
 *
 * @file src/common/Memory.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/* Copyright INRIA 2004
**
** This file is part of the Scotch distribution.
**
** The Scotch distribution is libre/free software; you can
** redistribute it and/or modify it under the terms of the
** GNU Lesser General Public License as published by the
** Free Software Foundation; either version 2.1 of the
** License, or (at your option) any later version.
**
** The Scotch distribution is distributed in the hope that
** it will be useful, but WITHOUT ANY WARRANTY; without even
** the implied warranty of MERCHANTABILITY or FITNESS FOR A
** PARTICULAR PURPOSE. See the GNU Lesser General Public
** License for more details.
**
** You should have received a copy of the GNU Lesser General
** Public License along with the Scotch distribution; if not,
** write to the Free Software Foundation, Inc.,
** 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
**
** $Id: common_memory.c 176 2004-10-12 13:53:26Z goureman $
*/
/*
  File: common_memory.c

  Part of a parallel direct block solver.

  This module handles errors.

  Authors:
    Mathieu  Faverge    - faverge@labri.fr
    Xavier   LACOSTE    - lacoste@labri.fr
    Francois PELLEGRINI - .

  Dates:
    Version 0.0  - from 07 sep 2001
                   to   07 sep 2001
    Version 0.1  - from 14 apr 2001
                   to   24 mar 2003
    Version 1.3  - from 25 feb 2004
                   to   25 feb 2004
    Version 2.0 (C++)
                 - from 16 dec 2009
                 - to   16 dec 2009
*/

/*
**  The defines and includes.
*/

#ifndef MEMORY_HPP
#define MEMORY_HPP

#ifdef MEMORY_USAGE

#include <cstdlib>
#ifdef VITE_DBG_MEMORY_TRACE
#include "common/Tools.hpp"
#include "common/TraceMemory.hpp"
#endif

/*
  Group: Variables

  The static variables.

  int: memallocmutexflag
    Boolean indicating if <memallocmutexdat> mutex has been initialized.

  pthread_mutex_t: memallocmutexdat
    mutex protecting <memalloccurrent>, <memallocmax>, <memalloctraceflag>,
    <trace_file>, <trace_timestamp> and <trace_procnum>

  ulong: memalloccurrent
    Current memory allocated using <memAlloc_func>.

  ulong: memallocmax
    Maximum value of memalloccurrent since the program started.

  int: memalloctraceflag
    Boolean indicating if we want to trace allocation.

  stream: trace_file
    File into which to write traces.

  double: time_stamp
    Origin of traces.

  int: trace_procnum
    Processor tracing allocations.
*/

extern "C" {

#ifdef VITE_DBG_MEMORY_TRACE
/*
  Function: memAllocTrace

  Start tracing memory.

  Initialize <memallocmutexdat> if not done.

  Defines all tracing variables.

  Parameters:
  file      - Stream where to write traces, opened in write mode.
  timestamp - Traces origin.
  procnum   - Processor writting traces.

  Returns:
  void - In all cases.
*/
void memAllocTrace(FILE *file,
                   double timestamp,
                   int procnum);

/*
  Function: memAllocUntrace

  Stop tracing allocations.

  Returns:
  void - in all cases.
*/
void memAllocUntrace();
#endif

/*
  Function: memAllocGetCurrent

  Get the current memory allocated.

  Returns:
  <memalloccurrent> value.
*/
unsigned long memAllocGetCurrent();

/*
  Function: memAllocGetMax

  Get the maximu memory allocated.

  Returns:
  <memallocmax> value.
*/
unsigned long memAllocGetMax();

/*
  Function: memAllocTraceReset

  Restarts tracing allocation with reseting <memallocmax>.

  Returns:
  void - in all cases.
*/
void memAllocTraceReset();
}

/*
   Function: operator new

   This is a thread-safe memory allocation routine.

   Parameters:
     size     - Memory size wanted.
     filename - Used for error message, file where the function is called.
     line     - Used for erro message, line where the function is called.

   Returns:
     !NULL - pointer to memory block.
     NULL  - no array allocated.
*/
void *operator new(std::size_t size);

/*
   Function: operator new[]

   This is a thread-safe memory allocation routine.

   Parameters:
     size     - Memory size wanted.
     filename - Used for error message, file where the function is called.
     line     - Used for erro message, line where the function is called.

   Returns:
     !NULL - pointer to memory block.
     NULL  - no array allocated.
*/
// void *operator new[](std::size_t size) throw(std::bad_alloc);

/*
   Function: operator delete

   This is a thread-safe memory deallocation routine.

   It returns:
     void - in all cases
*/
void operator delete(void *memptr);

/*
   Function: operator delete[]

   This is a thread-safe memory deallocation routine.

   It returns:
     void - in all cases
*/
// void operator delete[](void *memptr);

#endif /* MEMORY_USAGE */

#endif /* MEMORY_HPP */
