/**
 *
 * @file src/interface/RangeSliderWidget.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */
/*
 * This code is mostly inspired from :
 * https://github.com/sleepbysleep/range_slider_for_Qt5_and_PyQt5/blob/master/qt_range_slider/range_slider.h
 * Originated from
 * https://www.mail-archive.com/pyqt@riverbankcomputing.com/msg22889.html
 * Modification refered from
 * https://gist.github.com/Riateche/27e36977f7d5ea72cf4f
 *
 *
 *
 * This file define a RangeSliderWidget based on the QSlider Qt Widget
 * This slider can be Horizontal or Vertical
 * And it has a low handle and a high handle used
 * to define the low and high bounds of a range
 *
 */
#ifndef RANGE_SLIDER_WIDGET_HPP
#define RANGE_SLIDER_WIDGET_HPP

#include <QSlider>
#include <QStyle>

class RangeSliderWidget : public QSlider
{
    Q_OBJECT
public:
    RangeSliderWidget(Qt::Orientation ot = Qt::Orientation::Horizontal, QWidget *parent = nullptr);
    int low();
    void setLow(int low_limit);
    int high();
    void setHigh(int high_limit);
Q_SIGNALS:
    void sliderMoved(int, int);

private:
    int lowLimit, highLimit;

    int click_position;

    enum HANDLE { NO_HANDLE,
                  LOW_HANDLE,
                  HIGH_HANDLE };

    // Contains which handle is currently active
    HANDLE active_handle;

    int pixelPosToRangeValue(int pos);

    void paintEvent(QPaintEvent *ev) override;

    void mousePressEvent(QMouseEvent *ev) override;

    void mouseMoveEvent(QMouseEvent *ev) override;
};

#endif
