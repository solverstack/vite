/**
 *
 * @file src/interface/IntervalSelect.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file IntervalSelect.hpp
 */

#ifndef INTERVAL_SELECT_HPP
#define INTERVAL_SELECT_HPP

#include <QDialog>
/* -- */
#include "ui_interval_select.h"
/* -- */
#include "interface/IntervalSelectLayout.hpp"

/*!
 * \class IntervalSelect select
 * \brief Class used to select intervals on which to view traces
 *
 */

class IntervalSelect : public QDialog, protected Ui::interval_selector
{

    Q_OBJECT

private:
    /*!
     * \brief This variable contains the IntervalSelectLayouts that are inside this window.
     */
    std::vector<IntervalSelectLayout *> _interval_select_layouts;

public:
    /*!
     * Default constructor
     * \param parent The parent widget of the window.
     */
    IntervalSelect(QWidget *parent);

    ~IntervalSelect() override;

    /**
     * \brief Update IntervalSelectLayout container with currently visible render layouts
     * \param render_layouts Vector of currently displayed renders
     */
    void update_container(const std::vector<RenderLayout *> &render_layouts);

    /*!
     * \brief Set the slider and the box values linked with a render_layout to values from the view
     * \param render_layout A RenderLayout to know which slider to update
     */
    void update_interval_layout_from_render_view(const RenderLayout *render_layout);

    /**
     * \brief Allow the apply button to be pressed
     * Used by children to enable apply button when renders are not up to date
     */
    void allow_apply();

    /**
     * \brief Used by children to disable auto_refresh
     */
    void disable_auto_refresh();

    /**
     * \brief Used by children to know if auto refresh is enabled
     */
    bool is_auto_refresh_enabled();

private Q_SLOTS:

    /*!
     * \brief Apply settings on all renders
     */
    void on_apply_button_clicked();

    /*!
     * \brief Reset the children to default values
     */
    void on_reset_all_button_clicked();

    /*!
     * \brief Slot called when the auto_refresh checkbox is checked or unchecked
     */
    void on_auto_refresh_box_stateChanged();
};

#endif // INTERVAL_SELECT_HPP
