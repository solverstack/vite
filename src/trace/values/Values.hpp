/**
 *
 * @file src/trace/values/Values.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef VALUES_HPP
#define VALUES_HPP

/*!
 *
 * \file Values.hpp
 * \brief Contains the include of all the headers from this directory
 *
 */

#include "trace/values/Value.hpp"
#include "trace/values/Color.hpp"
#include "trace/values/Date.hpp"
#include "trace/values/Double.hpp"
#include "trace/values/Hex.hpp"
#include "trace/values/Integer.hpp"
#include "trace/values/String.hpp"
#include "trace/values/Name.hpp"

typedef enum EntityClass_e {
    _EntityClass_State,
    _EntityClass_Link,
    _EntityClass_Event,
    _EntityClass_Variable
} EntityClass_t;

#endif // VALUES_HPP
