/**
 *
 * @file src/trace/values/Color.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Arthur Redondy
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef COLOR_HPP
#define COLOR_HPP

/*!
 *
 * \file Color.hpp
 *
 */
/*!
 *
 * \class Color
 * \brief Store a color in the trace
 *
 */
class Color : public Value
{
private:
    /*! red value */
    double _r;
    /*! green value */
    double _g;
    /*! blue value */
    double _b;

    static double check_value(double, const std::string &);

public:
    /*!
     * \brief Constructor
     */
    Color();

    /*!
     * \brief Constructor
     */
    Color(double, double, double);
    Color(const std::string &);
    Color(const Color &);
    Color(const Color *);

    /*!
     * \fn to_string() const
     * \return a string of the color in the format "r g b".
     */
    std::string to_string() const override;

    /*!
     * \fn get_red() const
     * \return red value
     */
    double get_red() const;

    /*!
     * \fn get_green() const;
     * \return green value
     */
    double get_green() const;

    /*!
     * \fn get_blue() const;
     * \return blue value
     */
    double get_blue() const;
};

#endif // COLOR_HPP
