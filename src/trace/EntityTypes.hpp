/**
 *
 * @file src/trace/EntityTypes.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef ENTITYTYPES_HPP
#define ENTITYTYPES_HPP

#include "trace/values/Values.hpp"
#include "trace/ContainerType.hpp"
#include "trace/EntityType.hpp"
#include "trace/EventType.hpp"
#include "trace/StateType.hpp"
#include "trace/LinkType.hpp"
#include "trace/VariableType.hpp"

#endif
