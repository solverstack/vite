/**
 *
 * @file src/trace/Entity.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef ENTITY_HPP
#define ENTITY_HPP

/*!
 * \file Entity.hpp
 */
class Container;
/*!
 * \class Entity
 * \brief Abstract class that describe trace entities (events, states, links, variables)
 */
struct Entity
{
private:
    Container *_container = nullptr;
    std::map<std::string, Value *> *_extra_fields { nullptr };

public:
    /*
     * \fn Entity(Container *container, map<std::string, Value *> opt)
     * \brief Constructor
     * \param container A container
     * \param opt Optional fields of the container
     */
    Entity(Container *container, std::map<std::string, Value *> opt);
    Entity();

public:
    /*!
     * \fn get_container() const
     * \brief Get the container of the entity
     */
    const Container *get_container() const;

    //    /*!
    //     * \fn set_container(Container* cont) const
    //     * \brief sets the container of the entity
    //     */
    //    void set_container(Container* cont) {_container=cont;}
    //
    //    /*!
    //     * \fn set_extra_fields(std::map<std::string, Value *>* opt) const
    //     * \brief sets the container of the entity
    //     */
    //    void set_extra_fields(std::map<std::string, Value *>* opt) {_extra_fields=opt;}
    //
    /*!
     * \fn get_extra_fields() const
     * \brief Get the extra fields
     */
    const std::map<std::string, Value *> *get_extra_fields() const;

    ~Entity();

    void clear();
};

#endif
