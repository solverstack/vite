/**
 *
 * @file src/trace/SerializerDispatcher.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Thibault Soucarre
 *
 * @date 2024-07-17
 */

/*
 * \file SerializerDispatcher.hpp
 * Class that handles multiple threads for parallel serialization
 * A container calls its methods, which are executed on a free thread or polled if we need to wait
 */

#include <QObject>

#include "trace/Serializer.hpp"

struct IntervalOfContainer;
class SerializerWriter;
class SerializerDispatcher : public QObject, public Singleton<SerializerDispatcher>
{
    Q_OBJECT

    // the array of objects to send the data to (all of them have their own thread)
    SerializerWriter *_evt_array;
    // the index of the last one called, in order to avoid calling it just after
    unsigned int _previous_call;
    // the number of threads, usually the number of CPUs
    unsigned int _nb_threads;
    // check the state
    bool _killed;

    QMutex *_mutex;
    // static unsigned int nb_cpus=boost::thread::hardware_concurrency();

public:
    SerializerDispatcher();
    ~SerializerDispatcher();
    void init();
    void launch_threads();
    void kill_all_threads();
    // dump an itc, after choosing a free thread to use
    void dump(IntervalOfContainer *itc, char *);
    void load(IntervalOfContainer *, char *);
Q_SIGNALS:
    void load_data(IntervalOfContainer *, char *);
    void dump_on_disk(IntervalOfContainer *, char *);
    void build_finish();
};
