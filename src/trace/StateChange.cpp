/**
 *
 * @file src/trace/StateChange.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Augustin Degomme
 * @author Luigi Cannarozzo
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */

#include <string>
#include <map>
#include <list>
#include <vector>
#include <stack>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
/* -- */
using namespace std;

StateChange::StateChange() = default;

StateChange::StateChange(Date time) :
    _time(std::move(time)), _left(nullptr), _right(nullptr) {
}

StateChange::StateChange(Date time, State *left, State *right) :
    _time(std::move(time)), _left(left), _right(right) {
}

void StateChange::set_right_state(State *right) {
    _right = right;
}

Date StateChange::get_time() const {
    return _time;
}

State *StateChange::get_left_state() {
    return _left;
}

State *StateChange::get_right_state() {
    return _right;
}

const State *StateChange::get_left_state() const {
    return _left;
}

const State *StateChange::get_right_state() const {
    return _right;
}

StateChange::~StateChange() {
    delete _right;
    _right = nullptr;
}
void StateChange::clear() {
    if (_left != nullptr) {
        _left->clear();
        // free(_left_state);
        //  Alloc<State2>::Instance().free(_left_state);
    }
}
