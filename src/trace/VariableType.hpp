/**
 *
 * @file src/trace/VariableType.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Kevin Coulomb
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef VARIABLETYPE_HPP
#define VARIABLETYPE_HPP

/*!
 * \file VariableType.hpp
 */

#include "trace/ContainerType.hpp"
#include "trace/EntityType.hpp"

/*!
 * \class VariableType
 * \brief Describe the type of a variable
 */
class VariableType : public EntityType
{
public:
    /*!
     * \brief Constructor
     * \param name Name of the type
     * \param container_type Type of the container
     * \param opt optional fields
     */
    VariableType(const Name &name, ContainerType *container_type, std::map<std::string, Value *> opt);
    // VariableType();
};

#endif
