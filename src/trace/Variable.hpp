/**
 *
 * @file src/trace/Variable.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef VARIABLE_HPP
#define VARIABLE_HPP

#include "common/common.hpp"
/* -- */
#include "trace/VariableType.hpp"

/*!
 * \file Variable.hpp
 */

class Variable;

/*!
 * \class Variable
 * \brief Describe a variable
 */
class Variable : public Entity
{
private:
    std::list<std::pair<Date, Double>> _values;
    Double _min, _max;
    VariableType *_type;

public:
    /*!
     * \brief Constructor
     * \param container Container of this variable
     * \param type Type of this variable
     */
    Variable(Container *container, VariableType *type);

    /*!
     * \brief Destructor
     */
    ~Variable();

    /*!
     * \brief Add a new value to the variable
     * \param time Date of the new value
     * \param value The new value
     */
    void add_value(Date time, Double value);

    /*!
     * \brief Get the latest value of the variable
     */
    Double get_last_value() const;

    /*!
     * \brief Get the values of the variable
     */
    const std::list<std::pair<Date, Double>> *get_values() const;
    /*!
     * \brief Get the value at a given time
     */
    double get_value_at(const Times &d) const;

    /*!
     * \brief Get the minimum of the values
     */
    Double get_min() const;

    /*!
     * \brief Get the maximum of the values
     */
    Double get_max() const;

    /*!
     * \brief Get the type of the variable
     */
    VariableType *get_type() const;
};

#endif
