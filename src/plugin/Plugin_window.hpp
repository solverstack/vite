/**
 *
 * @file src/plugin/Plugin_window.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mohamed Faycal Boullit
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 *
 * @date 2024-07-17
 */

#ifndef PLUGINWINDOW_HPP
#define PLUGINWINDOW_HPP

#include <QObject>
#include <QMainWindow>

class Plugin;
class QPushButton;
class QTabWidget;
class QVBoxLayout;
class Core;

/*!
 * \class Plugin_window
 * \brief The window which shows all the plugins.
 */
class Plugin_window : public QMainWindow
{
    Q_OBJECT
private:
    QVBoxLayout *_layout;
    QTabWidget *_tab_widget;
    QPushButton *_execute_button;
    std::vector<Plugin *> _plugins;
    int _current_index;
    Interface_graphic *_interface_graphic;

    QStringList _plugin_directories;

public:
    /*!
     * \fn Plugin_window(Interface_graphic *interface_graphic, QWidget *parent = NULL)
     * \brief Constructor
     * \param c the Core
     * \param parent the parent widget
     */
    Plugin_window(Interface_graphic *interface_graphic);

    /*!
     * \fn ~Plugin_window()
     * \brief Destructor
     */
    ~Plugin_window() override;

    /*!
     * \fn load_list()
     * \brief Don't remember what this should do... maybe totally useless
     * maybe store the names of the plugins... Yep totally useless
     * \todo remove this useless function
     */
    void load_list();

    /*!
     * \fn load_plugin(const std::string &plugin_name)
     * \brief Load the static plugin.
     * \param plugin_name the name of the plugin we want to load
     */
    void load_plugin(const std::string &plugin_name);

    /*!
     * \fn update_trace()
     * \brief Modify the trace of all the plugins.
     */
    void update_trace();

    /*!
     * \fn update_render()
     * \brief Modify the render of all the plugins.
     */
    void update_render();

    /*!
     * \fn show()
     * \brief Show the window. Overriden to initialise the current plugin first.
     */
    void show();

    /*!
     * \fn clear_plugins()
     * \brief Call the clear method of all the loaded plugins.
     */
    void clear_plugins();

    /*!
     * \fn reload_plugins()
     * \brief Remove all the plugin and reload all the plugins.
     */
    void reload_plugins();

private:
    /*!
     * \fn load_shared_plugins()
     * \brief Load all the shared plugins that can be found in the directories set in the Settings module.
     */
    void load_shared_plugins();
    /*!
     * \fn add_plugin(Plugin *plug)
     * \brief Add a plugin to the list and to the window.
     * \param plug the plugin to add.
     */
    void add_plugin(Plugin *plug);

public Q_SLOTS:
    /*!
     * \fn reload_plugin(int index)
     * \brief Reload the trace of the index-th plugin
     * \param index the index of the plugin
     */
    void reload_plugin(int index);
    /*!
     * \fn execute_plugin()
     * \brief Execute the current plugin
     */
    void execute_plugin();
};

#endif // PLUGINWINDOW_HPP
