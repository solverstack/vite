/**
 *
 * @file src/render/vulkan/Vk_vertex_buffer.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Lucas Guedon
 *
 * @date 2024-07-17
 */
#ifndef VK_VERTEX_BUFFER_HPP
#define VK_VERTEX_BUFFER_HPP

#include <vector>
#include "render/vulkan/Vk_buffer.hpp"

class Vk_vertex_buffer : public Vk_buffer
{

private:
    uint32_t _vertex_count = 0;

public:
    Vk_vertex_buffer();

    uint32_t vertex_count();

    void bind_vertex_buffer(VkCommandBuffer *command_buffer);

    void draw(VkCommandBuffer *command_buffer);

    template <class V>
    void set_data(std::vector<V> data) {
        int size = data.size() * sizeof(V);
        if (_vertex_count != data.size()) {
            _vertex_count = data.size();
            realloc_buffer(size);
        }

        if (_vertex_count == 0)
            return;

        quint8 *buffer_data;
        VkResult err = _dev_funcs->vkMapMemory(_dev, _buf_mem, 0, size, 0, reinterpret_cast<void **>(&buffer_data));
        if (err != VK_SUCCESS)
            qFatal("Failed to map memory: %d", err);

        memcpy(buffer_data, data.data(), size);
        _dev_funcs->vkUnmapMemory(_dev, _buf_mem);
    }
};

#endif