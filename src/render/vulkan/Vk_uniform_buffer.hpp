/**
 *
 * @file src/render/vulkan/Vk_uniform_buffer.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Lucas Guedon
 *
 * @date 2024-07-17
 */
#ifndef VK_UNIFORM_BUFFER_HPP
#define VK_UNIFORM_BUFFER_HPP

#include "render/vulkan/Vk_buffer.hpp"

class Vk_uniform_buffer : public Vk_buffer
{

private:
    VkDescriptorSet _desc_set[QVulkanWindow::MAX_CONCURRENT_FRAME_COUNT];
    VkDescriptorBufferInfo _uniform_buf_info[QVulkanWindow::MAX_CONCURRENT_FRAME_COUNT];

public:
    Vk_uniform_buffer();

    /*!
     * \brief Creates the buffer on the GPU with the given size and initialises the content of the buffer by initialising the uniforms with identity matrices
     */
    void init_uniform(const VkDescriptorSetAllocateInfo *descriptor_info, uint32_t concurrent_frame_count, VkDeviceSize alloc_size);

    void set_variable(unsigned int frame_index, VkDeviceSize offset, VkDeviceSize size, const void *value);

    void bind_uniform_buffer(VkCommandBuffer &command_buffer, uint32_t frame_index, VkPipelineLayout &pipeline_layout);
};

#endif