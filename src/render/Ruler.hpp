/**
 *
 * @file src/render/Ruler.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file Ruler.hpp
 */

#ifndef RULER_HPP
#define RULER_HPP

/*!
 * \class Ruler
 * \brief Represents the displayed time ruler on the graphical interface.
 */
class Ruler
{

protected:
private:
    /*!
     * \brief The default constructor. In private scope to prevent instance.
     */
    Ruler() = default;

    /***********************************
     *
     * Ruler methods.
     *
     **********************************/
public:
    /*!
     * \brief Return the distance between two graduation in trace coordinates
     * \arg min The minimum time visible. (In trace coordinate.)
     * \arg max The maximum time visible. (In trace coordinate.)
     * \return Most suitable distance between two consecutive graduations within [min ; max].
     */
    static Element_pos get_graduation_diff(const Element_pos min, const Element_pos max);

    /*!
     * \brief Return a string containing the common first part from first_pos and second_pos
     * \param first_pos The first position to compare
     * \param second_pos The second position to compare
     */
    static std::string get_common_part_string(const Element_pos &first_pos, const Element_pos &second_pos);

    /*!
     * \brief Return a string of the variable part between graduation_pos and common_part_string
     * \param graduation_pos Position of the graduation
     * \param common_part_string Common part between graduations
     * \param length_after_point Number of digits after the floating point
     */
    static std::string get_variable_part(const Element_pos &graduation_pos, const std::string &common_part_string, const std::size_t &length_after_point);
};

#endif
