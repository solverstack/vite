/**
 *
 * @file src/statistics/Statistic.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

#include <string>
#include <map>
#include <list>
#include <vector>
#include <stack>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/EntityValue.hpp"
#include "trace/Entitys.hpp"
#include "trace/Container.hpp"
/* -- */
#include "statistics/Statistic.hpp"
/* -- */
using namespace std;

Statistic::Statistic() {
    _event = 0;
    _number_link = 0;
}

Statistic::~Statistic() {
    // Delete states
    for (auto &_state: _states) {
        delete _state.second;
    }
}

void Statistic::add_state(EntityValue const *ent, double length) {
    map<const EntityValue *, stats *>::iterator i = _states.find(ent);
    // If it does not exist, add a new entry
    if (i == _states.end()) {
        _states[ent] = new stats(length);
    }
    // Else update the state found
    else {
        (*i).second->_total_length += length;
        (*i).second->_cardinal++;
    }
}

void Statistic::add_link(const Container *cont) {
    map<const Container *, int>::iterator i = _link.find(cont);
    // If it does not exist, add a new entry
    if (i == _link.end()) {
        _link[cont] = 1;
    }
    // Else update the link to the container found
    else {
        (*i).second++;
        _number_link++;
    }
}

void Statistic::set_nb_event(int n) {
    _event = n;
}

int Statistic::get_nb_event() const {
    return _event;
}

map<const EntityValue *, stats *> Statistic::get_states() {
    return _states;
}
