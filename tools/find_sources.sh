#!/usr/bin/env sh
###
#
#  @file find_sources.sh
#  @copyright 2013-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @brief Generate the filelist for the static analysis
#
#  @version 1.4.0
#  @author Mathieu Faverge
#  @date 2023-11-22
#
###
if [ $# -gt 0 ]
then
    BUILDDIR=$1
fi
BUILDDIR=${BUILDDIR:-build}

echo $PWD
rm -f filelist.txt

git ls-files | grep "\.[ch]pp"   >  filelist.txt

# Remove all CMakeFiles generated file
sed -i '/CMakeFiles/d' filelist.txt

# Remove all external file
sed -i '/^external/d' filelist.txt

# Remove all .cmake files
sed -i '/.cmake/d' filelist.txt

# Remove all .in files
sed -i '/.in$/d' filelist.txt

# Remove all clang files
sed -i '/^\.clang/d' filelist.txt

# Remove installed files
sed -i '/^install.*/d' filelist.txt

grep '\.cpp$' filelist.txt > filelist-c.txt

