# Needs to set the VITEDIR environment VARIABLE before to run qmake
win32:TEMPLATE    = vclib
else:TEMPLATE     = lib
CONFIG      += plugin
TARGET       = TraceInfos
DEPENDPATH  += $(VITEDIR)/src .
INCLUDEPATH += $(VITEDIR)/src .
DESTDIR      = ~/.vite
OBJECTS_DIR  = $(VITEDIR)/bin
QT += xml

!win32:LIBS += -lrt

SOURCES      = TraceInfos.cpp 		\
	       trace/Trace.cpp          \
	       trace/values/Date.cpp    \
	       trace/values/Name.cpp    \
	       trace/values/Double.cpp  \
	       trace/values/String.cpp  \
	       trace/values/Color.cpp   \
	       trace/ContainerType.cpp  \
	       trace/EntityType.cpp     \
	       trace/VariableType.cpp   \
	       trace/EventType.cpp      \
	       trace/LinkType.cpp       \
	       trace/StateType.cpp      \
	       trace/Container.cpp      \
	       trace/Entity.cpp         \
	       trace/EntityValue.cpp    \
	       trace/Variable.cpp       \
	       trace/State.cpp          \
	       trace/StateChange.cpp    \
	       trace/Link.cpp           \
	       trace/Event.cpp          \
	       statistics/Statistic.cpp \
	       render/Palette.cpp       \
	       common/Session.cpp       \
	       common/Tools.cpp         \
	       common/Info.cpp
