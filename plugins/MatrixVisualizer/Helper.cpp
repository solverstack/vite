/**
 *
 * @file plugins/MatrixVisualizer/Helper.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 *
 * @date 2024-07-17
 */
#include "Helper.hpp"

#include <cstdarg>
#include <iostream>

#include "MatrixVisualizer.hpp"

namespace Helper {
void log(LogStatus status, const char *format, ...) {
    va_list ap;
    va_start(ap, format);

    Matrix_visualizer::get_instance()->log(status, format, ap);

    va_end(ap);
}

void set_infos(const char *format, ...) {
    va_list ap;
    va_start(ap, format);

    Matrix_visualizer::get_instance()->set_infos(format, ap);

    va_end(ap);
}
}
