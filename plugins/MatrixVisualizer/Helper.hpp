/**
 *
 * @file plugins/MatrixVisualizer/Helper.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef HELPER_HPP
#define HELPER_HPP

#define UNITS_COUNT 9 // Number of different unit types

enum LogStatus {
    MESSAGE,
    WARNING,
    FATAL,

    COUNT
};

static inline double
print_get_value(double flops) {
    static double ratio = (double)(1 << 10);
    int unit = 0;

    while ((flops > ratio) && (unit < UNITS_COUNT - 1)) {
        flops /= ratio;
        unit++;
    }
    return flops;
}

static inline char
print_get_units(double flops) {
    static char units[UNITS_COUNT] = { ' ', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y' };
    static double ratio = (double)(1 << 10);
    int unit = 0;

    while ((flops > ratio) && (unit < UNITS_COUNT - 1)) {
        flops /= ratio;
        unit++;
    }
    return units[unit];
}

namespace Helper {
void log(LogStatus status, const char *format, ...);
void set_infos(const char *format, ...);
}

#endif
